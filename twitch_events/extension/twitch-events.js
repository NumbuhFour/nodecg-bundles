const TwitchWebhook = require('twitch-webhook');
const axios = require('axios');

const creds = require('./creds.js');

const express = require('express');
const app = express();

module.exports = nodecg => {

  const twitchWebhook = new TwitchWebhook({
    client_id: creds.twitch.clientId,
    callback: creds.twitch.webhook.callback,
    secret:creds.twitch.webhook.secret,
    //secret: 'It\'s a secret', // default: false
    lease_seconds: 259200,    // default: 864000 (maximum value)
    listen: {
      port: creds.twitch.webhook.port,           // default: 8443
      host: creds.twitch.webhook.host,    // default: 0.0.0.0
      autoStart: false      // default: true
    }
  });

  // set listener for all topics
/*  twitchWebhook.on('*', ({ topic, options, endpoint, event }) => {
    // topic name, for example "streams"
    console.log(topic)
    // topic options, for example "{user_id: 12826}"
    console.log(options)
    // full topic URL, for example
    // "https://api.twitch.tv/helix/streams?user_id=12826"
    console.log(endpoint)
    // topic data, timestamps are automatically converted to Date
    console.log(event)
  })*/

  // set listener for topic
  twitchWebhook.on('streams', ({ event }) => {
    console.log(event)
  })
  twitchWebhook.on('subscribe', ({ event }) => {
    console.log('sub test')
  })
  // set listener for topic
  twitchWebhook.on('users/follows', ({ event }) => {
    console.log("GOT A FOLLOWER", event)
  })

  //Can't start a listen server using twitch-webhooks. Add a nodecg route instead and send to library
  app.get('/webhooks/twitch', (req,res) => {
    //res.send('OK!');
    console.log('Got Webhook', req.url);
    twitchWebhook._requestListener(req,res);
  });
  app.post('/webhooks/twitch', (req,res) => {
    //res.send('OK!');
    console.log('Got POST Webhook', req.url, 'BODY', req.body);
    twitchWebhook._requestListener(req,res, req.body);
  });
  nodecg.mount(app);

  // subscribe to topic
  twitchWebhook.subscribe('streams', {
    user_id: creds.twitch.targets.numbuhfour // ID of Twitch Channel ¯\_(ツ)_/¯
  })

  // subscribe to topic
  twitchWebhook.subscribe('users/follows', {
    first: 1,
    from_id: creds.twitch.targets.numbuhfour // ID of Twitch Channel ¯\_(ツ)_/¯
  })

  // renew the subscription when it expires
  twitchWebhook.on('unsubscribe', (obj) => {
    twitchWebhook.subscribe(obj['hub.topic'])
  })

	const githubResultsReplicant = nodecg.Replicant('github-results');

	nodecg.listenFor('findRepositories', async query => {
		nodecg.log.info(`Extension received the value ${query}!`);

		try {
			const apiResponse = await axios.get('https://api.github.com/search/repositories', {
				params: {
					q: query
				}
			});

			nodecg.log.info(`Found ${apiResponse.data.total_count} results from the github api!`);

			githubResultsReplicant.value = apiResponse.data.items;
		} catch (error) {
			nodecg.log.error(error);
		}
	});


  // tell Twitch that we no longer listen
  // otherwise it will try to send events to a down app
  process.on('SIGINT', () => {
    console.log('Unsubscribing from twitch events');
    // unsubscribe from all topics
    process.exit(0);
  });



  var rl;
  if (process.platform === "win32") {
    var rl = require("readline").createInterface({
      input: process.stdin,
      output: process.stdout
    });
  }
  // catching signals and do something before exit
  ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
      'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM', 'SIGUR2'
  ].forEach(function (sig) {
    process.on(sig, function () {
      terminator(sig);
      console.log('signal: ' + sig);
    });
    //Windows is fucky with its exit codes
    if (process.platform === "win32") {
      rl.on(sig, function () {
        terminator(sig);
        console.log('signal: ' + sig);
      });
    }
  });

  function terminator(sig) {

    console.log('Unsubscribing from twitch webhooks');
    twitchWebhook.unsubscribe('*')
    if (typeof sig === "string") {
      // call your async task here and then call process.exit() after async task is done
      console.log('Received %s - terminating server app ...', sig);
      process.exit(1);
    }
  }

}

process.on('beforeExit', () => {console.log('beforexit')});
