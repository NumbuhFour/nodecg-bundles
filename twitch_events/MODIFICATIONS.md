In order to get the twitch webhook library to work, I had to make a few modifications to it and nodecg so that it does not have to run a second listening server

## /node_modules/twitch-webhook/src/index.js

Edited the function `_processUpdates` to use the raw post data gathered by
express' web-parser.
This involved moving the `request.on('end')` callback into a named function
and calling it when `request.rawBody` was not undefined

```
const endFunc = () => {
  let data
  if (request.rawBody)
    body = request.rawBody;
  try {
    data = JSON.parse(body)
  } catch (err) {
    this.emit('webhook-error', new errors.WebhookError('JSON is malformed'))
    response.writeHead(202, { 'Content-Type': 'text/plain' })
    response.end()
    return
  }

  if (this._options.secret) {
    let storedSign = crypto
      .createHmac('sha256', this._subscriptions[endpoint].secret)
      .update(body)
      .digest('hex')

    if (storedSign !== signature) {
      this.emit('webhook-error', new errors.WebhookError('"x-hub-signature" is incorrect'))
      response.writeHead(202, { 'Content-Type': 'text/plain' })
      response.end()
      return
    }
  }

  response.writeHead(200, { 'Content-Type': 'text/plain' })
  response.end()

  //if (request.body) data = request.body;

  let payload = {}
  payload.topic = topic
  payload.options = options
  payload.endpoint = endpoint
  payload.event = this._fixDate(topic, data)

  this.emit(topic, payload)
  this.emit('*', payload)
}

// Since we're inserting via express, we can't read the post body directly
// instead, we insert the parsed body via parsedData
if (request.body) endFunc();
else
  request.on('end', endFunc);
```

## nodecg/lib/server.js

replaced

```
app.use(bodyParser.json());
```

with
```
app.use(bodyParser.json({
  verify: (req, res, buf) => {
    req.rawBody = buf; // Ensure raw body data is saved
  }
}));
```

in order to pass raw post data through to the twitch webhook library
